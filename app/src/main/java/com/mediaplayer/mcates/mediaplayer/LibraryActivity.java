package com.mediaplayer.mcates.mediaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class LibraryActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);

        ImageView back = findViewById(R.id.back_arrow);
        back.setOnClickListener(this);

        LinearLayout cellOne = findViewById(R.id.cell1);
        cellOne.setOnClickListener(this);
        LinearLayout cellTwo = findViewById(R.id.cell2);
        cellTwo.setOnClickListener(this);
        LinearLayout cellThree = findViewById(R.id.cell3);
        cellThree.setOnClickListener(this);
        LinearLayout cellFour = findViewById(R.id.cell4);
        cellFour.setOnClickListener(this);
        LinearLayout cellFive = findViewById(R.id.cell5);
        cellFive.setOnClickListener(this);
        LinearLayout cellSix = findViewById(R.id.cell6);
        cellSix.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent playlistIntent = new Intent(LibraryActivity.this, PlaylistActivity.class);
        switch (v.getId()) {

            case R.id.cell1:
                startActivity(playlistIntent);
                break;

            case R.id.cell2:
                startActivity(playlistIntent);
                break;

            case R.id.cell3:
                startActivity(playlistIntent);
                break;

            case R.id.cell4:
                startActivity(playlistIntent);
                break;

            case R.id.cell5:
                startActivity(playlistIntent);
                break;

            case R.id.cell6:
                startActivity(playlistIntent);
                break;

            case R.id.back_arrow:
                finish();
                break;
        }
    }
}
