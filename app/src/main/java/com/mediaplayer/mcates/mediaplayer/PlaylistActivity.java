package com.mediaplayer.mcates.mediaplayer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;


public class PlaylistActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist);

        ImageView back = findViewById(R.id.back_arrow);
        back.setOnClickListener(this);

        RelativeLayout one = findViewById(R.id.playlist_row_1);
        one.setOnClickListener(this); // Calls the onClick
        RelativeLayout two = findViewById(R.id.playlist_row_2);
        two.setOnClickListener(this);
        RelativeLayout three = findViewById(R.id.playlist_row_3);
        three.setOnClickListener(this);
        RelativeLayout four = findViewById(R.id.playlist_row_4);
        four.setOnClickListener(this);
        RelativeLayout five = findViewById(R.id.playlist_row_5);
        five.setOnClickListener(this);
        RelativeLayout six = findViewById(R.id.playlist_row_6);
        six.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent playerIntent = new Intent(PlaylistActivity.this, PlayerActivity.class);
        switch (v.getId()) {

            case R.id.playlist_row_1:
                startActivity(playerIntent);
                break;

            case R.id.playlist_row_2:
                startActivity(playerIntent);
                break;

            case R.id.playlist_row_3:
                startActivity(playerIntent);
                break;

            case R.id.playlist_row_4:
                startActivity(playerIntent);
                break;

            case R.id.playlist_row_5:
                startActivity(playerIntent);
                break;

            case R.id.playlist_row_6:
                startActivity(playerIntent);
                break;

            case R.id.back_arrow:
                finish();
                break;

            default:
                break;
        }
    }
}